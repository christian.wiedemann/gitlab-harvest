'use strict';

chrome.runtime.onInstalled.addListener(details => {
  // display the previous install
  if (typeof details !== 'undefined' && typeof details.previousVersion !== 'undefined') {
    console.log('previousVersion', details.previousVersion);
  }

  // get the config
  chrome.storage.sync.get(
    {'hosts': ['https://gitlab.com', 'https://github.com'],
    'projects': {'example/website' : 'EXAMPLE-01'},
    'show_widget': 'no'},
    function (conf) {
      // define explicitly to lessen malicious or broken configs
      var params = {
        hosts: conf.hosts,
        projects: conf.projects,
        show_widget: conf.show_widget
      };

      // write the default conf now
      chrome.storage.sync.set(params, function () {
      });
    }
  );
});
