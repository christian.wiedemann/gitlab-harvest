'use strict';
/* jshint browser: true */

// Saves options to chrome.storage.sync.
function save_options() {
  var status = document.getElementById('status');

  status.innerHTML = '';
  status.className = '';
  try {
    var hosts = widgets['hosts'].getValues();
    var project_values = widgets['projects'].getValues();
    var projects = {};
    for (var i in project_values) {
      projects[project_values[i][0]] = project_values[i][1];
    }
    var show_widget = document.getElementById('show_widget');

    var params = {
      hosts: hosts,
      projects: projects,
      show_widget: show_widget.value
    };

    chrome.storage.sync.set(params, function () {
      // Update status to let user know options were saved.
      status.innerHTML = 'Options saved.';
      status.className = 'success';
      setTimeout(function () {
        status.className = 'hidden';
      }, 5000);
    });
  } catch (exeption) {
    status.innerHTML = 'Invalid options.<br>';
    status.innerHTML = exeption.message;
    status.className = 'error';
    setTimeout(function () {
      status.className = 'hidden';
    }, 5000);
  }
}

// Restores preferences stored in chrome.storage.
function restore_options() {

  // Use default value hosts = 'gitlab.com,github.com'.
  chrome.storage.sync.get({ 'hosts': ['https://gitlab.com', 'https://github.com'],
    'projects': { 'example/website': 'EXAMPLE-01' },
    'show_widget': 'no' }, function (conf) {
      var show_widget = document.getElementById('show_widget');
      show_widget.value = conf.show_widget;

      // Build hosts configuration widget.
      for (var i = 0; i < conf.hosts.length; i++) {
        widgets['hosts'].addRow([conf.hosts[i]]);
      }

      // Build mapping configuration widget.
      for (var key in conf.projects) {
        widgets['projects'].addRow([key, conf.projects[key]]);
      }
  });
}

document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click', save_options);
